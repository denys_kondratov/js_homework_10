/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

/*
1. Зєжнати верстку з джс 
Потрібно зробити цей калькулятор робочим.
2. Знайти всі кнопки та спрбувати їх вивести у консоль
3. Записти перші числа в память коду 
4. вивести числа на екран
5. додати знаки ар. операцій 
6. Знайти другі числа 
7. Вивести другі числа 
8. Вивести результат операції
*/

const calculate = {
    operand1 : "",
    sign : "",
    operand2 : "",
    rez : "",
    mem: ""
}
const rezult = document.getElementById("res");
const querySelector = document.querySelector(".display input");

// https://regexr.com/

document.querySelector(".keys").addEventListener("click", (e) => {
    if (validate(/[0-9.]/, e.target.value) && calculate.sign ==='' && e.target.value !== '=' && calculate.rez ==='') {
        calculate.operand1 += e.target.value;
        show(calculate.operand1);        
    } else if (querySelector.value === "ERROR" && validate(/[0-9.]/, e.target.value)) {
        querySelector.value = "";
        calculate.operand1 += e.target.value;
        show(calculate.operand1);
    } else if (validate(/[+-/*]/, e.target.value) && e.target.value !== '.' && e.target.value !== '=' && e.target.value !== 'C' && e.target.value !== 'm-' && e.target.value !== 'm+' && calculate.operand1 !== '' && calculate.operand2 == '') {
        calculate.sign = e.target.value;
    } else if (validate(/[0-9.]/, e.target.value) && calculate.operand1 !=='' && calculate.sign !=='' && e.target.value !== '=' && e.target.value !== 'C') {
        calculate.operand2 += e.target.value;
        rezult.removeAttribute("disabled");
        show(calculate.operand2);
    } else if (validate(/^=$/, e.target.value)) {
        if (calculate.sign == "+") {
            calculate.rez = Number(calculate.operand1) + Number(calculate.operand2);
        } else if (calculate.sign == "-") {
            calculate.rez = calculate.operand1 - calculate.operand2;
        } else if (calculate.sign == "*") {
            calculate.rez = calculate.operand1 * calculate.operand2;
        } else if (calculate.sign == "/" && calculate.operand2 != '0') {
            calculate.rez = calculate.operand1 / calculate.operand2;
        } else {
            calculate.rez = "ERROR";            
            calculate.operand2 = "";
            calculate.sign = "";
            rezult.setAttribute("disabled", "disabled");
            calculate.operand1 = "";           
            console.log("error");
            show(calculate.rez);
            return;
        }        
        calculate.operand1 = calculate.rez;
        calculate.operand2 = "";
        calculate.sign = "";
        rezult.setAttribute("disabled", "disabled");
        show(calculate.rez);
    } else if (calculate.operand1 !== '' && calculate.operand2 !== '' && calculate.sign !== '' && validate(/[+-/*]/, e.target.value) && e.target.value !== '.') {
        if (calculate.sign == "+") {
            calculate.rez = Number(calculate.operand1) + Number(calculate.operand2);
        } else if (calculate.sign == "-") {
            calculate.rez = calculate.operand1 - calculate.operand2;
        } else if (calculate.sign == "*") {
            calculate.rez = calculate.operand1 * calculate.operand2;
        } else if (calculate.sign == "/" && calculate.operand2 != '0') {
            calculate.rez = calculate.operand1 / calculate.operand2;
        } else {
            calculate.rez = "ERROR";            
            calculate.operand2 = "";
            calculate.sign = "";
            rezult.setAttribute("disabled", "disabled");
            calculate.operand1 = "";           
            console.log("error");
            show(calculate.rez);
            return;
        }        
        calculate.operand1 = calculate.rez;
        calculate.operand2 = "";
        calculate.sign = e.target.value;
        rezult.setAttribute("disabled", "disabled");
        show(calculate.rez);
    } else if (validate(/^C$/, e.target.value)) {
        calculate.operand1 = "";
        calculate.operand2 = "";
        calculate.sign = "";
        calculate.rez = "";
        rezult.setAttribute("disabled", "disabled");
        show(calculate.rez);
    } else if (querySelector.value !=='' && calculate.mem =='' && e.target.value == 'm+') {
        calculate.mem = querySelector.value;
        querySelector.classList.add("display_m");
    } else if (querySelector.value !=='' &&  calculate.mem !== "" && e.target.value == 'm+') {
        calculate.mem = Number(calculate.mem) + Number(calculate.operand1);      
    } else if (calculate.mem !== '' && e.target.value == 'mrc') {
        calculate.operand1 = calculate.mem;
        show(calculate.mem);
    } else if (querySelector.value == calculate.mem && e.target.value == 'm-' && querySelector.value !=='') {
        calculate.mem = "";
        calculate.operand1 = "";
        calculate.operand2 = "";
        calculate.sign = "";
        calculate.rez = "";
        rezult.setAttribute("disabled", "disabled");
        querySelector.classList.remove("display_m");
        show(calculate.rez);
    }
});


document.addEventListener("keypress", (e) => {
    //console.dir(e);    
    if (validate(/[0-9.]/, String.fromCharCode(e.charCode)) && calculate.sign ==='' && calculate.rez ==='') {
        calculate.operand1 += String.fromCharCode(e.charCode);
        show(calculate.operand1);        
    } else if (querySelector.value === "ERROR" && validate(/[0-9.]/, String.fromCharCode(e.charCode))) {
        querySelector.value = "";
        calculate.operand1 += String.fromCharCode(e.charCode);
        show(calculate.operand1);
    } else if (validate(/[+-/*]/, String.fromCharCode(e.charCode)) && String.fromCharCode(e.charCode) !== '.' && calculate.operand1 !== '' && calculate.operand2 == '') {
        calculate.sign = String.fromCharCode(e.charCode);
    } else if (validate(/[0-9.]/, String.fromCharCode(e.charCode)) && calculate.operand1 !=='' && calculate.sign !=='' && String.fromCharCode(e.charCode) !== '=' && e.charCode !== 13) {
        calculate.operand2 += String.fromCharCode(e.charCode);
        rezult.removeAttribute("disabled");
        show(calculate.operand2);
    } else if ((calculate.operand1 !== '' && calculate.operand2 !== '' && calculate.sign !== '' && validate(/^=$/, String.fromCharCode(e.charCode))) || (calculate.operand1 !== '' && calculate.operand2 !== '' && calculate.sign !== '' && validate(/^13$/, e.which)) || (calculate.operand1 !== '' && calculate.operand2 !== '' && calculate.sign !== '' && validate(/^13$/, e.which))) {
        e.preventDefault();
        if (calculate.sign == "+") {
            calculate.rez = Number(calculate.operand1) + Number(calculate.operand2);
        } else if (calculate.sign == "-") {
            calculate.rez = calculate.operand1 - calculate.operand2;
        } else if (calculate.sign == "*") {
            calculate.rez = calculate.operand1 * calculate.operand2;
        } else if (calculate.sign == "/" && calculate.operand2 != '0') {
            calculate.rez = calculate.operand1 / calculate.operand2;
        } else {
            calculate.rez = "ERROR";            
            calculate.operand2 = "";
            calculate.sign = "";
            rezult.setAttribute("disabled", "disabled");
            calculate.operand1 = "";           
            console.log("error");
            show(calculate.rez);
            return;
        }        
        calculate.operand1 = calculate.rez;
        calculate.operand2 = "";
        calculate.sign = "";
        rezult.setAttribute("disabled", "disabled");
        show(calculate.rez);
    } else if (calculate.operand1 !== '' && calculate.operand2 !== '' && calculate.sign !== '' && validate(/[+-/*]/, String.fromCharCode(e.charCode)) && String.fromCharCode(e.charCode) !== '.') {
        if (calculate.sign == "+") {
            calculate.rez = Number(calculate.operand1) + Number(calculate.operand2);
        } else if (calculate.sign == "-") {
            calculate.rez = calculate.operand1 - calculate.operand2;
        } else if (calculate.sign == "*") {
            calculate.rez = calculate.operand1 * calculate.operand2;
        } else if (calculate.sign == "/" && calculate.operand2 != '0') {
            calculate.rez = calculate.operand1 / calculate.operand2;
        } else {
            calculate.rez = "ERROR";            
            calculate.operand2 = "";
            calculate.sign = "";
            rezult.setAttribute("disabled", "disabled");
            calculate.operand1 = "";           
            console.log("error");
            show(calculate.rez);
            return;
        }        
        calculate.operand1 = calculate.rez;
        calculate.operand2 = "";
        calculate.sign = String.fromCharCode(e.charCode)
        rezult.setAttribute("disabled", "disabled");
        show(calculate.rez);
    }
}, true);

function show (v) {
    const d = document.querySelector(".display input");
    d.value = v;
}

const validate = (r, v) => r.test(v);